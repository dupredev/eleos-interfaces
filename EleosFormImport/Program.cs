﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Net.Mail;

namespace EleosFormImport
{
    class Program
    {

        static void Main(string[] args)
        {
            #region  Email Setup
            //Setup email configuration
            ////SmtpClient SmtpServer = new SmtpClient("smtpdomino.duprelogistics.com");
            ////MailMessage mail = new MailMessage();
            ////mail.From = new MailAddress("noreply@duprelogistics.com");
            ////SmtpServer.Port = 25;
            #endregion

            #region Sql Server Connections
            //  Connect to Database for Read
            var ReadDataConnection = new SqlConnection();
            ReadDataConnection = ConnectToDatabase();
            ReadDataConnection.Close();
            ReadDataConnection.Open();

            //  Connect to Database for header insert
            var HeaderInsertDataConnection = new SqlConnection();
            HeaderInsertDataConnection = ConnectToDatabase();
            HeaderInsertDataConnection.Close();
            HeaderInsertDataConnection.Open();

            //  Connect to database for deatil insert 
            var DetailInsertDataConnection = new SqlConnection();
            DetailInsertDataConnection = ConnectToDatabase();
            DetailInsertDataConnection.Close();
            DetailInsertDataConnection.Open();
            #endregion

            #region Sql Statements
            //  Setup insert header command
            var InsertHeaderStmt = "insert into Dupre.Eleos_MsgHistHeader (EleosHandle, CreatedDt, DriverId, FormCode) " +
                "values(@EleosHandle, @CreatedDt, @DriverId, @FormCode)";
            var InsertHeaderCmnd = new SqlCommand(InsertHeaderStmt, HeaderInsertDataConnection);
            InsertHeaderCmnd.Parameters.Add("@EleosHandle", SqlDbType.VarChar, 45, "EleosHandle");
            InsertHeaderCmnd.Parameters.Add("@CreatedDt", SqlDbType.DateTime2, 6, "CreatedDt");
            InsertHeaderCmnd.Parameters.Add("@DriverId", SqlDbType.VarChar, 8, "DriverId");
            InsertHeaderCmnd.Parameters.Add("@FormCode", SqlDbType.VarChar, 255, "FormCode");

            //  Setup insert detail command
            var InsertDetailStmt = "insert into Dupre.Eleos_MsgHistDetail (EleosHandle, SeqNo, Heading, Value) " +
                "values(@EleosHandle, @SeqNo, @Heading, @Value)";
            var InsertDetailCmnd = new SqlCommand(InsertDetailStmt, DetailInsertDataConnection);
            InsertDetailCmnd.Parameters.Add("@EleosHandle", SqlDbType.VarChar, 45, "EleosHandle");
            InsertDetailCmnd.Parameters.Add("@SeqNo", SqlDbType.Int, 10, "SeqNo");
            InsertDetailCmnd.Parameters.Add("@Heading", SqlDbType.VarChar, 100, "Heading");
            InsertDetailCmnd.Parameters.Add("@Value", SqlDbType.VarChar, 1000, "Value"); 
            #endregion

            //  Setup message reader command
            var GetMessagesStmt = "select * from ELEOS.ASR_Eleos_MessageHistory where EleosHandle not in (select EleosHandle from Dupre.Eleos_MsgHistHeader)";
            var GetMessagesCmnd = new SqlCommand(GetMessagesStmt, ReadDataConnection);

            //  Open message reader
            var MessagesReader = GetMessagesCmnd.ExecuteReader();
            string[] HeadingValuePair;
            while (MessagesReader.Read())
            {
                //  Write header record
                InsertHeaderCmnd.Parameters["@EleosHandle"].Value = MessagesReader["EleosHandle"];
                InsertHeaderCmnd.Parameters["@CreatedDt"].Value = MessagesReader["CreatedDt"];
                InsertHeaderCmnd.Parameters["@DriverId"].Value = MessagesReader["DriverId"];
                InsertHeaderCmnd.Parameters["@FormCode"].Value = MessagesReader["FormCode"];
                ////InsertHeaderCmnd.Parameters["@ReviewedBy"].Value = null;
                ////InsertHeaderCmnd.Parameters["@ReviewedDateTime"].Value = null;
                InsertHeaderCmnd.ExecuteNonQuery();

                //  Convert contents to field list
                string Contents = MessagesReader["Contents"].ToString();
                string[] ContentValues = Contents.Split(',');

                //  Write detail records
                var SeqNumber = 0;
                foreach (var Pair in ContentValues)
                {
                    var Heading = " ";
                    var Value = " ";
                    HeadingValuePair = Pair.Split('=');
                    if (HeadingValuePair.Count() <= 1)
                    {
                        Heading = "Text";
                        Value = HeadingValuePair[0];
                    }
                    else
                    {
                        Heading = HeadingValuePair[0];
                        Value = HeadingValuePair[1];
                    }
                    SeqNumber += 1;
                    InsertDetailCmnd.Parameters["@EleosHandle"].Value = MessagesReader["EleosHandle"];
                    InsertDetailCmnd.Parameters["@SeqNo"].Value = SeqNumber;
                    InsertDetailCmnd.Parameters["@Heading"].Value = Heading;
                    InsertDetailCmnd.Parameters["@Value"].Value = Value;
                    InsertDetailCmnd.ExecuteNonQuery();

                }

                //  Check for notification request
                ////if (MessagesReader["FormCode"].ToString() == "MASTER-DATA-CORRECTION")
                ////{
                ////    mail.To.Add("crudeoiloperations@duprelogistics.com");
                ////    mail.Subject = ("Master Data Correction");
                ////    mail.Body = string.Empty;
                ////    mail.Body = $"Created By: {MessagesReader["DriverId"]} On: {MessagesReader["CreatedDt"]} ";
                ////    var counter = 0;
                ////    foreach (var Pair in ContentValues)
                ////    {
                ////        var Heading = " ";
                ////        var Value = " ";
                ////        HeadingValuePair = Pair.Split('=');
                ////        if (HeadingValuePair.Count() <= 1)
                ////        {
                ////            Heading = "Text";
                ////            Value = HeadingValuePair[0];
                ////        }
                ////        else
                ////        {
                ////            Heading = HeadingValuePair[0];
                ////            Value = HeadingValuePair[1];
                ////        }
                ////        ////if (counter > 0) mail.Body = mail.Body + "\r\n" + Heading + "=" + Value;
                ////        if (mail.Body != string.Empty) mail.Body = mail.Body + "\r\n" + Heading + "=" + Value;
                ////        else mail.Body = Heading + "=" + Value;
                ////        counter += 1;
                ////    }
                ////    SmtpServer.Send(mail);
                ////}

            }
            ReadDataConnection.Close();
            HeaderInsertDataConnection.Close();
            DetailInsertDataConnection.Close();

        }
        public static SqlConnection ConnectToDatabase()
        {
            ////var connectionString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            var connectionString = ConfigurationManager.ConnectionStrings["EleosLive"].ConnectionString;
            return new SqlConnection(connectionString);
        }
    }
}
